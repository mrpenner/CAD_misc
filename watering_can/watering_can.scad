$fn=60;
thickness=2;
height=100;
base=50;
slope=0.9;
spout_l=90;
spout_d=15;
spout_angle=-30;
difference(){
    union(){
        cylinder(r1=base, r2=base*slope, h=height, center=true);
        translate([-(base-spout_d), 0, -(height/2-spout_d)])rotate([0, spout_angle, 0])
        cylinder(r1=spout_d, r2=spout_d*.7, h=spout_l, center=false);
        translate([-55, 0, 40])rotate([90, 43, 0])flower();
        //handle
        translate([base-17, 0, 0])rotate([90, 0, 0])
        difference(){
            cylinder(r=height/2, h=10, center=true);
            cylinder(r=height/2-10, h=11, center=true);
        }
    }
    translate([0, 0, thickness])
    cylinder(r1=base-thickness, r2=base*slope-thickness, h=height, center=true);
    translate([-(base-spout_d), 0, -(height/2-spout_d)])rotate([0,  spout_angle, 0])
    cylinder(r1=spout_d-thickness, r2=spout_d*.7-thickness, h=spout_l+0.1, center=false);
}
module flower(){
    radius=3;
    depth=3;
    for(i=[1:5]){
        rotate([ 0, 0, 72*i])
        hull(){
            cylinder(r=radius*0.4, h=depth, center=true);
            translate([radius*3, 0, 0])cylinder(r=radius, h=depth, center=true);
        }
    }
}
