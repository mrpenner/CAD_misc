Stand for BeagleBone Black and screen
======================================

3d printable stand to hold screen and mount BeagleBone Black to the back of the screen. It is designed for this screen_. Written in OpenSCAD, so should not be to hard to modify for other screens.

------

.. image:: assembled_back_view.jpg
	   :align: center

.. image:: assembled_front_view.jpg
	   :align: center

.. _screen: https://www.sparkfun.com/products/16006
