// system to mount BeagleBone Black to the back of a screen
// with stand

$fn=100;
w_inner=125;
h_inner=80;
bezel=10;
bezel_r=5;
w_outer=w_inner+bezel*2;
h_outer=h_inner+bezel*2;
scr_bolt_xyz=[for(x=[bezel/2, w_outer-bezel/2],
                  y=[bezel/2, h_outer-bezel/2])
              [x, y, 0]];
scr_bolt=3.5;

// uncomment lines below to see assembly
//color("lime")screen_mount();
//translate([0, 0, 9])color("yellow")bbb_mount();

// uncomment lines below for a printable layout
color("lime")screen_mount();
translate([0, h_outer+10, 0])color("yellow")bbb_mount();

module screen_mount(){
    th=3;
    standoff_h=9;
    standoff_d=bezel;
    nut_h=standoff_h-th;
    nut_d=8.1;
    handle_w=85;
    handle_o=20;
    handle_r=handle_o/2+5;
    scr_a=15; // angle back from vertical
    stand_w=50;
    difference(){
        union(){
            // bezel and handle
            hull(){
                for(x=[bezel_r, w_outer-bezel_r], y=[bezel_r, h_outer-bezel_r])
                translate([x, y, 0]) cylinder(h=th, r=bezel_r);
                for(x=[w_outer/2-handle_w/2+handle_o/2,
                       w_outer/2+handle_w/2-handle_o/2])
                translate([x, h_outer+handle_o/2, 0])
                cylinder(h=th, r=handle_r);
            }
            // standoffs with buttresses
            // the if statements are to orient the buttresses for each corner
            for(xyz=scr_bolt_xyz) translate(xyz){
                hull(){
                    cylinder(h=standoff_h, d=standoff_d);
                    if(xyz[0]==bezel/2){
                        translate([standoff_h, 0, th]) sphere(d=0.1);
                    } else {
                        translate([-standoff_h, 0, th]) sphere(d=0.1);
                }}
                hull(){
                    cylinder(h=standoff_h, d=standoff_d);
                    if(xyz[1]==bezel/2){
                        translate([0, standoff_h, th]) sphere(d=0.1);
                    } else {
                        translate([0, -standoff_h, th]) sphere(d=0.1);
            }}}
            // stand
            translate([0, -2, 0]) // move stand down so standoffs don't hit surface
            for(x=[bezel/2, w_outer-bezel/2]){
                hull(){
                    translate([x, stand_w*cos(scr_a), th])
                    rotate([270, 0, 0])
                    cylinder(h=stand_w/4, d1=th*2, d2=0.1);
                    translate([x, stand_w*sin(scr_a), stand_w*cos(scr_a)])
                    rotate([0, 90, 0])
                    cylinder(h=th, d=th, center=true);
                }
                // foot on main frame
                hull()for(y=[0, 2]){
                    translate([x==bezel/2 ? bezel : w_outer-bezel, y, th/2])
                    rotate([0, 90, 0])
                    cylinder(h=stand_w/5, d=th, center=true);
        }}}
        // cut out for the back of the screen
        translate([bezel, bezel, -0.1]) cube([w_inner, h_inner, th+0.2]);
        // bolt holes and nut recesses
        for(xyz=scr_bolt_xyz) translate(xyz){
            translate([0, 0, th]) cylinder(h=standoff_h, d=scr_bolt);
            translate([0, 0, -0.01])
                cylinder($fn=6, h=nut_h, d=nut_d/cos(30));
        }
        // handle slot
        hull(){
            for(x=[w_outer/2-handle_w/2+handle_o/2, w_outer/2+handle_w/2-handle_o/2])
            translate([x, h_outer+handle_o/2, 0])
            cylinder(h=th*3, d=handle_o, center=true);
}}}

module bbb_mount(){
    // piece to mount BeagleBone Black to the screen mount
    standoff_h=8;
    nut_h=standoff_h-3;
    nut_d=4.2;
    standoff_d=nut_d/cos(30)+1.5;
    bolt=2.2;
    support_h=standoff_h-4;
    support_w=standoff_d*0.75;
    scr_tab_d=scr_bolt+3;
    width=54;
    length=86;
    bbb_xyz=[w_outer/2-length/2, h_outer-bezel/2-width, 0];
    relative_bolt_xyz=[[5, 6, 0],
                       [5, width-6, 0],
                       [length-15, 3, 0],
                       [length-15, width-3, 0]];
    bolt_xyz=[for(i=relative_bolt_xyz)bbb_xyz+i];
    // bolt positions are numbered bottom left, top left, bottom right, top right
    bl=0;
    tl=1;
    br=2;
    tr=3;

    difference(){
        union(){
            for(xyz=scr_bolt_xyz) translate(xyz)
               cylinder(h=support_h, d=scr_tab_d);
            for(xyz=bolt_xyz) translate(xyz){
                cylinder(h=standoff_h, d=standoff_d);
                cylinder(h=support_h, d1=standoff_d+1, d2=standoff_d);
            }
            for(i=[[bl, tl],
                   [tl, tr],
                   [tr, br],
                   [br, bl]])
                hull(){
                    translate(bolt_xyz[i[0]])
                    cylinder(h=support_h, d1=support_w, d2=0.25);
                    translate(bolt_xyz[i[1]])
                    cylinder(h=support_h, d1=support_w, d2=0.25);
                }
            // draw supports angling from screen bolt tabs to bbb bolt standoffs
            for(support=[[bl, bl],
                         [bl, br],
                         [tl, bl],
                         [tl, tl],
                         [br, bl],
                         [br, br],
                         [tr, br],
                         [tr, tr]]){
                hull(){
                    translate(scr_bolt_xyz[support[0]])
                    cylinder(h=support_h, d1=support_w, d2=0.25);
                    translate(bolt_xyz[support[1]])
                    cylinder(h=support_h, d1=support_w, d2=0.25);
        }}}
        // screen tabs holes
        for(xyz=scr_bolt_xyz) translate(xyz)
            cylinder(h=support_h*3, d=scr_bolt, center=true);
        // bbb bolt holes and nut recesses
        for(xyz=bolt_xyz) translate(xyz){
            cylinder(h=standoff_h*3, d=bolt, center=true);
            translate([0, 0, -0.01])
                cylinder($fn=6, h=nut_h, d=nut_d/cos(30));
}}}
