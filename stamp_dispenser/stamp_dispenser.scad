// dispenser for postage stamps on a roll
stamp_d=37;
stamp_h=30;
wall=2;
align_d=(stamp_d+wall*2)*0.25;
snap_d=8; // diameter
snap_ball=6; // radius
clearance=1;
snap_hole=snap_ball*2-wall/2;
snap_hollow=snap_ball+clearance;
$fn=60;
render()
union(){
    difference(){  // main body
        union(){
            translate([wall/2,wall/2,wall/2])
            minkowski(){
                sphere(wall/2);
                cube([stamp_d+wall, stamp_d+wall, stamp_h]);
            }
            // cylinders to align lid
            translate([stamp_d/2+wall, stamp_d/2+wall, stamp_h+wall-0.1])
            for(i=[-1:2:1]){
                translate([i*(stamp_d/2-align_d/2), 0, 0])
                for(i=[-1:2:1]){
                    translate([0, i*(stamp_d/2-align_d/2), 0])
                    cylinder(d=align_d, h=wall);
                }
            }
        }
        translate([stamp_d/2+wall, stamp_d/2+wall, wall]){
            // hole for the roll of stamps
            cylinder(d=stamp_d, h=stamp_h+wall);
            for(i=[0:90:91]){
                rotate([0,0,i])
                translate([stamp_d/2+wall+0.01, 0, stamp_h/2])
                rotate([0,-90,0])
                if(i==90){ // holes in the sides
                    difference(){
                        cylinder(d=stamp_d*0.6, h=stamp_d+wall*2.1);
                        translate([-stamp_h/2,0,wall*2])
                        cube([stamp_h, stamp_d/2, wall]); // to keep from cutting out the point
                    }
                }else{
                    cylinder(d=stamp_d*0.6, h=stamp_d+wall*2.1);
                }
            }
        }
        translate([-0.01, stamp_d, wall])
        cube([stamp_d/2+wall, wall, stamp_h+wall]); // slot to feed end of stamp roll out
    }
    difference(){  // center snap-fit post
        union(){
            translate([stamp_d/2+wall, stamp_d/2+wall, 0])
            cylinder(d=snap_d, h=(stamp_h+wall*2)-snap_ball);
            translate([stamp_d/2+wall, stamp_d/2+wall, (stamp_h+wall*2)-snap_ball])
            sphere(snap_ball);
        }
        translate([stamp_d/2+wall, stamp_d/2+wall, stamp_h+wall*2])
        for(i=[0:90:91]){ // snap-fit slots in post and ball
            rotate([0, 0, i])
            hull(){
                rotate([90, 0, 0])
                cylinder(d=wall*1.1, h=snap_ball*2.02, center=true);
                translate([0, 0, -stamp_h*0.75])
                rotate([90, 0, 0])
                cylinder(d=wall*0.6, h=snap_ball*2.02, center=true);
            }
        }
    }
}
// snap-fit lid
render()
translate([stamp_d+wall*2+5, 0, 0]){
    difference(){
        union(){
            translate([wall/2,wall/2,wall/2])
            minkowski(){
                sphere(wall/2);
                cube([stamp_d+wall, stamp_d+wall, 0.1]);
            }
            translate([stamp_d/2+wall, stamp_d/2+wall, 0])
            intersection(){
                translate([0, 0, snap_ball])
                sphere(snap_hollow+wall);
                translate([-(snap_hollow+wall), -(snap_hollow+wall), 0])
                cube((snap_hollow+wall)*2); // so the sphere does not extend above the lid
            }
        }
        translate([stamp_d/2+wall, stamp_d/2+wall, -0.1])
        cylinder(d=snap_hole, h=stamp_h);
        translate([stamp_d/2+wall, stamp_d/2+wall, snap_ball])
        sphere(snap_hollow);
        translate([stamp_d/2+wall, stamp_d/2+wall, -0.1])
        // holes to align with body
        for(i=[-1:2:1]){
            translate([i*(stamp_d/2-(align_d+clearance)/2), 0, 0])
            for(i=[-1:2:1]){
                translate([0, i*(stamp_d/2-(align_d+clearance)/2), 0])
                cylinder(d=align_d+clearance, h=wall+0.2);
            }
        }
    }
}