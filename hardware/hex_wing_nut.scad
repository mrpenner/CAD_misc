hex=14.5;
hex_h=6.5;
tot_h=14;
$fn=50;
difference(){
    for(i=[0:180:359]){
        hull(){
            // circumscribe the points of the hex
            cylinder(h=tot_h, d=hex*1.1/cos(30));
            // the end of the wing
            rotate([0,0,i])translate([hex*1.5/cos(30),0,0])
            cylinder(h=tot_h*0.75, d=hex*.25);
        }
    }
    translate([0,0,tot_h-hex_h])
    cylinder($fn=6, h=hex_h+0.1, d=hex/cos(30));
}
