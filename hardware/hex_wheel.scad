hex=7;
hex_h=3;
height=6;
$fn=50;
difference(){
    for(i=[0:60:359]){ // for each point of the hexagon
        hull(){
            // circumscribe the points of the hex
            cylinder(h=height, d=hex/cos(30)*1.05);
            // cylinder around the point
            rotate([0,0,i])translate([hex/cos(30)/2,0,0])
            cylinder(h=height, d=hex*.3);
        }
    }
    translate([0,0,height+0.01-hex_h])
    cylinder($fn=6, h=hex_h, d=hex/cos(30));
}
