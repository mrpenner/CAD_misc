# Miscellaneous CAD and 3D printable designs

This repository is for various objects I have designed. Most of them are designed to be 3D printed. I primarily use [OpenSCAD](https://openscad.org/) or [FreeCAD](https://www.freecadweb.org/).
