// sight tube for collimating a Newtonian reflector telescope
o_d=31.75; // to fit in focuser
f_r=5; // f/ratio
wall_t=2; // wall thickness
pupil=1.6;
height=(o_d-wall_t*2)*f_r; // inside diameter * f/ratio
$fn=100;

difference(){
    cylinder(h=height, d=o_d);
    translate([0,0,wall_t])
    cylinder(h=height, d=o_d-wall_t*2);
    cylinder(h=wall_t*3, d=pupil, center=true);
    // cross hairs
    translate([0,0,height]){
        for(i=[0:90:359]){
            rotate([0,0,i])translate([o_d/2,0,0]){
                // space to glue strings
                cube([wall_t,wall_t*2,wall_t*1.5], center=true);
                // string slots
                rotate([0,-90,0])
                hull(){
                    cylinder(h=wall_t*2, d=wall_t/2);
                    translate([-wall_t*.75,0,0])
                    cylinder(h=wall_t*2, d=.2);
                }
            }
        }
    }
}
