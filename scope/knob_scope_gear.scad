$fn=50;
include<MCAD/involute_gears.scad>

gear (
	number_of_teeth=10,
	circular_pitch=400,
	pressure_angle=28,
	clearance = 0.2,
	gear_thickness=8.1,
	rim_thickness=8.1,
	hub_thickness=8.1,
	bore_diameter=6,
	involute_facets=0,
	flat=false);
translate ([0, 0, 15.5]) 
    for(i=[1:5]){
        rotate([0,0,72*i])translate([15,0,0])
                cylinder(r=8, h=15, center=true);
    }
translate ([0, 0, 15.5])
    cylinder(r=15, h=15, center=true);
    