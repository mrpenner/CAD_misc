$fn=50;
include<MCAD/involute_gears.scad>

gear (
	number_of_teeth=100,
	circular_pitch=300,
	pressure_angle=28,
	clearance = 0.2,
	gear_thickness=3,
	rim_thickness=8,
	rim_width=5,
	hub_thickness=4,
	hub_diameter=15,
	bore_diameter=10,
	circles=5,
	backlash=0,
	twist=0,
	involute_facets=0,
	flat=false);
    
translate([100, 0, 0])    
  gear (
	number_of_teeth=10,
	circular_pitch=300,
	pressure_angle=28,
	clearance = 0.2,
	gear_thickness=10,
	rim_thickness=8,
	rim_width=2,
	hub_thickness=5,
	hub_diameter=15,
	bore_diameter=6,
	circles=0,
	backlash=0,
	twist=0,
	involute_facets=0,
	flat=false);