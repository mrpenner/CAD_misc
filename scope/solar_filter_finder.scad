// solar filter holder for a finderscope
$fn=60;
dia=58;
clear_bot=0.8; // clearance at end of scope
clear_top=1.2; // a little extra clearance at mouth
depth=30;
thickness=2;
flange=10;
inner_flange=4;
render()
difference(){
    union(){
        cylinder(r1=(dia+clear_bot)/2+thickness, r2=(dia+clear_top)/2+thickness, h=depth+thickness);
        cylinder(r=dia/2+flange, h=thickness);
    }
    translate([0, 0, thickness])
    cylinder(r1=(dia+clear_bot)/2, r2=(dia+clear_top)/2, h=depth+0.1);
    translate([0, 0, -0.1])
    cylinder(r=dia/2-inner_flange, h=thickness+0.1);
}
translate([4+dia+(flange+thickness)*2, 0, 0])
// piece to hold filter material (glued to main piece)
render()
difference(){
    cylinder(r=dia/2+thickness+flange+1, h=thickness*2);
    translate([0, 0, thickness])
    cylinder(r=dia/2+flange+1, h=thickness);
    translate([0, 0, -0.1])
    cylinder(r=dia/2, h=thickness+0.1);
}
